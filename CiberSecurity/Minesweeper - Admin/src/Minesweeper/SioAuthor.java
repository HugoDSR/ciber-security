/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Minesweeper;

import Bibliotecas.Cifras.Cifras;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 *
 * @author hugo
 */
public class SioAuthor {

    //public static String path = "/home/hugo/Dropbox/SIo/";
    public static String path = "/home/tiagoteixeira/Dropbox/SIO/Minesweeper-Licenca/";

    public static void main(String[] args) {
        String pedidoRegistoPath = path + "PedidoRegisto/";
        String licencaPath = path + "Licenca/";
        File folder = new File(pedidoRegistoPath);
        File[] listRoot = folder.listFiles();
        for (File dir : listRoot) {
            String pathR = pedidoRegistoPath + dir.getName() + "/";
            String pathL = licencaPath + dir.getName() + "/";
            if (autenticarUser(pathR)) {

                createDirectory(pathL);

                getADecifra(pathR);
                getSDecifra(pathR, pathL);

                addDataExpiracao(pathL);

                setSCifra(pathL);
                setACifra(pathL, pathR);

                        
                assinarFile(pathL);
                
                File delete = new File(pathR);
                removeDirectory(delete);
            }
        }
    }

    
    public static void assinarFile(String path) {
        FileInputStream fileIn = null;
        try {
            String certificado = path + "/Certificado";
            String assinatura = path + "/Assinatura";
            
            fileIn = new FileInputStream(path + "/Licenca");
            byte[] registo = new byte[fileIn.available()];
            fileIn.read(registo);
            Cifras.assinarFile(registo, certificado, assinatura);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileIn.close();
            } catch (IOException ex) {
                Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static boolean autenticarUser(String path) {
        FileInputStream registFile = null;
        FileInputStream assinaturaFile = null;
        boolean validar = false;
        try {
            registFile = new FileInputStream(path + "Regist");
            byte[] message = new byte[registFile.available()];
            registFile.read(message);

            assinaturaFile = new FileInputStream(path + "Assinatura");
            byte[] assinatura_ficheiro = new byte[assinaturaFile.available()];
            assinaturaFile.read(assinatura_ficheiro);

            validar = Cifras.verificarAss(message, assinatura_ficheiro, path + "Certificado");

        } catch (FileNotFoundException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                registFile.close();
            } catch (IOException ex) {
                Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return validar;
    }

    public static void getADecifra(String path) {
        FileInputStream keyFile = null;
        FileInputStream privKey = null;
        try {
            keyFile = new FileInputStream(path + "Key");
            byte[] key = new byte[keyFile.available()];
            keyFile.read(key);

            privKey = new FileInputStream("privKeyFile");
            byte[] privateKey = new byte[privKey.available()];
            privKey.read(privateKey);

            Cifras.aDecifrar(key, privateKey, "sKey");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                keyFile.close();
            } catch (IOException ex) {
                Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static void getSDecifra(String pathPedido, String pathRegisto) {

        FileInputStream keyFile = null;
        FileInputStream file = null;
        try {
            keyFile = new FileInputStream("sKey");
            byte[] key = new byte[keyFile.available()];
            keyFile.read(key);

            file = new FileInputStream(pathPedido + "Regist");
            byte[] regist = new byte[file.available()];
            file.read(regist);

            Cifras.sDecifrar(regist, key, pathRegisto + "Licenca");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                keyFile.close();
            } catch (IOException ex) {
                Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static void setSCifra(String pathRegisto) {
        FileInputStream licencaFile = null;
        try {
            SecretKey key = Cifras.gerarKey();
            licencaFile = new FileInputStream(pathRegisto + "Licenca");
            byte[] licenca = new byte[licencaFile.available()];
            licencaFile.read(licenca);

            Cifras.sCifrar(licenca, key, pathRegisto + "Licenca");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                licencaFile.close();
            } catch (IOException ex) {
                Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void setACifra(String pathRegisto, String pathPedido) {
        FileInputStream fileIn = null;
        FileInputStream fileIn2 = null;
        try {
            fileIn = new FileInputStream("sKeyFile");
            byte[] cif = new byte[fileIn.available()];
            fileIn.read(cif);
            fileIn2 = new FileInputStream(pathPedido+"PublicKey");
            byte[] pubKey = new byte[fileIn2.available()];
            fileIn2.read(pubKey);
            Cifras cifra = new Cifras();
            Cifras.aCifrarPubic(cif, pubKey, pathRegisto + "Key");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileIn.close();
                fileIn2.close();
            } catch (IOException ex) {
                Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void createDirectory(String dir) {
        File theDir = new File(dir);
        if (!theDir.exists()) {
            theDir.mkdir();
        }
    }

    public static void removeDirectory(File dir) {
        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            if (files != null && files.length > 0) {
                for (File aFile : files) {
                    removeDirectory(aFile);
                }
            }
            dir.delete();
        } else {
            dir.delete();
        }
    }

    public static void addDataExpiracao(String path) {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(path + "Licenca");
            Element root = document.getDocumentElement();

            Element newElement = document.createElement("Licenca");

            Element name = document.createElement("DataExpira");

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, 30);

            name.appendChild(document.createTextNode(dateFormat.format(cal.getTime())));
            newElement.appendChild(name);

            root.appendChild(newElement);

            DOMSource source = new DOMSource(document);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            StreamResult result = new StreamResult(path + "Licenca");
            transformer.transform(source, result);
        } catch (TransformerException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SioAuthor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
