package Minesweeper;

import Bibliotecas.Sio.Library;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Menu extends JPanel implements ActionListener {

    private JButton jogar = new JButton("Jogar");
    private JButton registar = new JButton("Registar");
    private JButton infoLicenca = new JButton("Info Registo");
    private Library library;

    public Menu() {
        library = new Library("Minesweeper", "1.2");
        jogar.setPreferredSize(new Dimension(200, 75));
        registar.setPreferredSize(new Dimension(200, 75));
        infoLicenca.setPreferredSize(new Dimension(200, 75));
        jogar.setFont(new Font("Tahoma", Font.BOLD, 20));
        registar.setFont(new Font("Tahoma", Font.BOLD, 20));
        infoLicenca.setFont(new Font("Tahoma", Font.BOLD, 20));

        add(jogar);
        add(registar);
        add(infoLicenca);

        jogar.addActionListener(this);
        registar.addActionListener(this);
        infoLicenca.addActionListener(this);
        
        
    }

    public void actionPerformed(ActionEvent evt) {
        Object source = evt.getSource();
        Color color = getBackground();
        if (source == jogar) {
            if (library.isRegistered()) {
                Minesweeper.play();
            } else {
                JOptionPane.showMessageDialog(null, "Tem que se registar para jogar");
                library.startRegistration();
            }
        } else if (source == registar) {
            library.startRegistration();
        } else if (source == infoLicenca) {
            library.showLicenseInfo();
        }
    }

}
