package Minesweeper;

import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Minesweeper");
        frame.setSize(300, 335);
        frame.setLocationRelativeTo(null);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        Container contentPane = frame.getContentPane();
        contentPane.add(new Menu());

        frame.show();
        
        PathDropbox.path =  JOptionPane.showInputDialog(null,"Indique o caminho da"
                            + " pasta partilhada da DropBox. Exemplo:  "
                            + "\n /home/username/Dropbox/SIO/");
        if(PathDropbox.path.charAt(PathDropbox.path.length()-1) != '/') {
            PathDropbox.path = PathDropbox.path + "/";
        }
        createDirectory(PathDropbox.path + "Minesweeper-Licenca");
        createDirectory(PathDropbox.path + "Minesweeper-Licenca/PedidoRegisto");
        createDirectory(PathDropbox.path + "Minesweeper-Licenca/Licenca");
    }
    
    public static void createDirectory(String dir) {
        File theDir = new File(dir);
        if (!theDir.exists()) {
            theDir.mkdir();
        }
    }
}
