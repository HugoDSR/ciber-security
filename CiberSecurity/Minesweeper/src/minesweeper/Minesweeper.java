package Minesweeper;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.InputEvent;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

public class Minesweeper extends JFrame {

    private int columns = 10;
    private int rows = 10;
    private static final long serialVersionUID = 1L;
    boolean jBombs[][] = new boolean[rows][columns];
    boolean jShown[][] = new boolean[rows][columns];
    int jCells[][] = new int[rows][columns];
    private int currX, currY = 0;
    JToggleButton jButtons[] = new JToggleButton[columns * rows];
    private JPanel jPanel = null;
    private JToolBar jToolBar = null;
    private JPanel jContentPane = null;
    private JButton jBtnNewGame = null;
    private JProgressBar jProgressBar = null;

    public Minesweeper() {
        super();
        initialize();
    }

    public static void play() {
        Minesweeper m = new Minesweeper();
        m.setVisible(true);
        m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void initialize() {
        this.setSize(436, 315);
        this.setContentPane(getJPanel());
        this.setLocationRelativeTo(null);
        this.setTitle("Minesweeper");
    }

    private JPanel getJPanel() {
        if (jPanel == null) {
            jPanel = new JPanel();
            jPanel.setLayout(new BorderLayout());
            jPanel.add(getJToolBar(), BorderLayout.NORTH);
            jPanel.add(getJContentPane(), BorderLayout.CENTER);
            jPanel.add(getJProgressBar(), BorderLayout.SOUTH);
        }
        return jPanel;
    }

    private JToolBar getJToolBar() {
        if (jToolBar == null) {
            jToolBar = new JToolBar();
            jToolBar.setFloatable(false);
            jToolBar.add(getJBtnNewGame());
        }
        return jToolBar;
    }

    private JPanel getJContentPane() {
        if (jContentPane == null) {
            GridLayout gridLayout = new GridLayout();
            gridLayout.setRows(rows);
            gridLayout.setColumns(columns);
            jContentPane = new JPanel();
            jContentPane.setLayout(gridLayout);

            BuildBoard();
        }

        return jContentPane;
    }

    private void BuildBoard() {
        // JContentPane will call this function before jProgressBar was built when first started
        if (jProgressBar != null) // So we check to see if the progress bar has been initialized
        {
            jProgressBar.setValue(0);
        }
        jContentPane.removeAll();
        int i = 0;
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < columns; y++) {
                currX = x;
                currY = y;
                Random randBomb = new Random();
                jBombs[x][y] = randBomb.nextBoolean() && randBomb.nextBoolean() && randBomb.nextBoolean(); // 13% chances of a bomb
                jButtons[i] = new JToggleButton("");
                jButtons[i].addMouseListener(new java.awt.event.MouseAdapter() {
                    public void mouseReleased(java.awt.event.MouseEvent e) {
                        if (e.getModifiers() == InputEvent.BUTTON3_MASK) {
                            markCell(e);
                        } else if (e.getModifiers() == InputEvent.BUTTON1_MASK) {
                            showCell(e);
                        }
                    }
                });
                jContentPane.add(jButtons[i]);
                i++;
            }
        }

        // Build the board
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < columns; y++) {
                jCells[x][y] = bombCount(x, y);
                jShown[x][y] = false; // Reset previous values
            }
        }
        jContentPane.setEnabled(true);
        this.repaint();
        this.validate();
    }

    private JButton getJBtnNewGame() {
        if (jBtnNewGame == null) {
            jBtnNewGame = new JButton();
            jBtnNewGame.setText("New Game");
            jBtnNewGame.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseReleased(java.awt.event.MouseEvent e) {
                    BuildBoard();
                }
            });
        }
        return jBtnNewGame;
    }

    private JProgressBar getJProgressBar() {
        if (jProgressBar == null) {
            jProgressBar = new JProgressBar();
            jProgressBar.setMaximum(columns * rows);
        }
        return jProgressBar;
    }

    private void showAllBombs() {
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < columns; y++) {
                if (jBombs[x][y] == true) {
                    JToggleButton jButton = findButton(x, y);
                    if (jButton.isEnabled()) // Don't go over the ones that were already counted
                    {
                        jProgressBar.setValue(jProgressBar.getValue() + 1);
                    }
                    jButton.setText("X");
                    jButton.setSelected(true);
                    jButton.setEnabled(false);
                }
            }
        }
    }

    private void clearCells(int x, int y) {
        // If the cell is in bounds
        if (inBounds(x, y)) {
            if (!jShown[x][y] && jBombs[x][y] == false) {
                jShown[x][y] = true;
                JToggleButton jButton = findButton(x, y);
                if (jCells[x][y] > 0) {
                    jButton.setText(Integer.toString(jCells[x][y]));
                } else {
                    jButton.setText("");
                }
                if (jButton.isEnabled()) // Don't go over the ones that were already counted
                {
                    jProgressBar.setValue(jProgressBar.getValue() + 1);
                }
                jButton.setSelected(true);
                jButton.setEnabled(false);

                // Check surrounding cells
                if (jCells[x][y] == 0) {
                    for (int r = -1; r <= 1; r++) {
                        for (int c = -1; c <= 1; c++) {
                            clearCells(x + r, y + c);
                        }
                    }
                }
            }
        }
    }

    private boolean inBounds(int x, int y) {
        // Check if position is within bounds
        return 0 <= x && x < jCells.length && 0 <= y && y < jCells[x].length;
    }

    private boolean isBomb(JToggleButton jButton) {
        int i = 0;
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < columns; y++) {
                if (jButton == jButtons[i]) {
                    currX = x;
                    currY = y;
                    return jBombs[x][y];
                }
                i++;
            }
        }
        return false;
    }

    private void disableBoard() {
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < columns; y++) {
                JToggleButton jButton = findButton(x, y);
                jButton.setEnabled(false);
            }
        }
    }

    private JToggleButton findButton(int x, int y) {
        return jButtons[(x * rows + y)];
    }

    private void showCell(java.awt.event.MouseEvent e) {
        JToggleButton jButton = (JToggleButton) e.getSource();
        if (jButton.isEnabled()) {
            jProgressBar.setValue(jProgressBar.getValue() + 1);
            jButton.setEnabled(false);

            if (isBomb(jButton)) {
                showAllBombs();
                jButton.setEnabled(false);
                JOptionPane.showMessageDialog(null, "You lost " + Math.round((jProgressBar.getPercentComplete() * 100)) + "% through.", "You Lost!", JOptionPane.INFORMATION_MESSAGE);
                disableBoard();
            } else if (jCells[currX][currY] > 0) {
                jButton.setText(Integer.toString(jCells[currX][currY]));
            } else if (jCells[currX][currY] == 0) {
                clearCells(currX, currY);
            }
        }
    }

    private int bombCount(int x, int y) {
        int bombCount = 0;

        // Count bombs in surrounding cells
        for (int r = -1; r <= 1; r++) {
            for (int c = -1; c <= 1; c++) {
                int newx = x + r;
                int newy = y + c;
                if (inBounds(newx, newy)) {
                    if (jBombs[newx][newy] == true) {
                        bombCount++;
                    }
                }
            }
        }
        return bombCount;
    }

    private void markCell(java.awt.event.MouseEvent e) // To set and remove the "!" flag
    {
        JToggleButton jButton = (JToggleButton) e.getSource();
        if (jButton.isEnabled()) {
            if (jButton.getText() != "!") {
                jButton.setText("!");
            } else {
                jButton.setText("");
            }
        }
    }
}
