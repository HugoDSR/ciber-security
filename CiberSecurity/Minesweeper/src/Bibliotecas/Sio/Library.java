package Bibliotecas.Sio;

import Bibliotecas.Cifras.Cifras;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import Minesweeper.PathDropbox;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Library {

    private String nomeDaApp;
    private String versao;
    private String nomeUser;
    private String bi;
    private String mac;
    private String machineId;
    private String sintese;
    private String data;

    public Library(String nomeDaApp, String versao) {
        this.nomeDaApp = nomeDaApp;
        this.versao = versao;
    }

    public boolean isRegistered() {
        boolean registado = false;
        if (Cifras.isSmartCardIn()) {
            File folder = new File(PathDropbox.path + "Minesweeper-Licenca/Licenca/");
            File[] listRoot = folder.listFiles();
            for (File dir : listRoot) {
                if (dir.getName().equals(Cifras.getNumeroCC())) {
                    registado = true;
                    break;
                }
            }
            if (registado) {
                String path = PathDropbox.path + "Minesweeper-Licenca/Licenca/" + Cifras.getNumeroCC() + "/";
                registado = autenticarAutor(path);
                if (registado) {
                    decifrarFiles(path);
                    registado = verificarLicenca();
                    clean();
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Tem que inserir o cardão de cidadão para jogar");
            isRegistered();
        }
        return registado;
    }

    public boolean startRegistration() {
        if (Cifras.isSmartCardIn()) {
            Register frame = new Register(getMac(), getMachineId());
            //frame.init();
            nomeUser = frame.getTxtNome();
            bi = frame.getTxtBI();
            mac = getMac();
            machineId = getMachineId();
        } else {
            JOptionPane.showMessageDialog(null, "Tem que inserir o cardão de cidadão para iniciar o registo");
        }

        return true;
    }

    public void showLicenseInfo() {
        if (Cifras.isSmartCardIn()) {
            File folderLicenca = new File(PathDropbox.path + "Minesweeper-Licenca/Licenca/" + Cifras.getNumeroCC());
            File folderPedido = new File(PathDropbox.path + "Minesweeper-Licenca/PedidoRegisto/" + Cifras.getNumeroCC());
            if(folderLicenca.exists()) {
                String path = PathDropbox.path + "Minesweeper-Licenca/Licenca/" + Cifras.getNumeroCC() + "/";
                decifrarFiles(path);
                readLicenca();
                clean();
                JOptionPane.showMessageDialog(null, this.toString());
            } else if(folderPedido.exists()) {
                JOptionPane.showMessageDialog(null, "Pedido de registo a aguardar a confirmação do administrador.");
            } else {
                JOptionPane.showMessageDialog(null, "Não existe nenhum registo.");
            }
            
        } else {
            JOptionPane.showMessageDialog(null, "Tem que inserir o cardão de cidadão para prosseguir");
        }
    }

    public boolean autenticarAutor(String path) {
        FileInputStream licencaFile = null;
        FileInputStream assinaturaFile = null;
        boolean validar = false;
        try {
            licencaFile = new FileInputStream(path + "Licenca");
            byte[] message = new byte[licencaFile.available()];
            licencaFile.read(message);

            assinaturaFile = new FileInputStream(path + "Assinatura");
            byte[] assinatura_ficheiro = new byte[assinaturaFile.available()];
            assinaturaFile.read(assinatura_ficheiro);

            validar = Cifras.verificarAss(message, assinatura_ficheiro, path + "Certificado");

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, ex);
        }

        return validar;
    }

    public void decifrarFiles(String path) {
        try {
            FileInputStream keyFile = new FileInputStream(path + "Key");
            byte[] key = new byte[keyFile.available()];
            keyFile.read(key);

            FileInputStream privKey = new FileInputStream("PrivateKey");
            byte[] publicKey = new byte[privKey.available()];
            privKey.read(publicKey);

            Cifras.aDecifrarPriv(key, publicKey, "sKey");

            keyFile = new FileInputStream("sKey");
            key = new byte[keyFile.available()];
            keyFile.read(key);

            FileInputStream file = new FileInputStream(path + "Licenca");
            byte[] regist = new byte[file.available()];
            file.read(regist);

            Cifras.sDecifrar(regist, key, "Licenca");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void clean() {
        File file = new File("Licenca");
        file.delete();
        file = new File("sKey");
        file.delete();
    }

    public boolean verificarLicenca() {
        boolean verificar = false;
        try {
            int tolerancia = 0;
            File inputFile = new File("Licenca");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("Registo");
            String mac = "";
            String machineid = "";
            String nome = "";
            String bi = "";
            String sintese = "";
            String data = "";
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    mac = eElement.getElementsByTagName("Mac").item(0).getTextContent();
                    machineid = eElement.getElementsByTagName("Machine-id").item(0).getTextContent();
                    nome = eElement.getElementsByTagName("Nome").item(0).getTextContent();
                    bi = eElement.getElementsByTagName("BI").item(0).getTextContent();
                    sintese = eElement.getElementsByTagName("Sintese").item(0).getTextContent();
                    data = eElement.getElementsByTagName("DataExpira").item(0).getTextContent();
                }
            }
            if (!mac.equals(getMac()) && tolerancia == 0) {
                JOptionPane.showMessageDialog(null, "Algo foi alterado em relação à licença! Desta vez será tolerado.");
                tolerancia++;
            } else if (tolerancia > 0) {
                return false;
            }

            if (!machineid.equals(getMachineId()) && tolerancia == 0) {
                JOptionPane.showMessageDialog(null, "Algo foi alterado em relação à licença! Desta vez será tolerado.");
                tolerancia++;
            } else if (tolerancia > 0) {
                JOptionPane.showMessageDialog(null, "Mais coisas foram alteradas. Lincença inválida!");
                return false;
            }
            if (!nome.equals(Cifras.getNomeCC())) {
                JOptionPane.showMessageDialog(null, "Lincenca invalida!");
                return false;
            }

            if (!bi.equals(Cifras.getNumeroCC())) {
                JOptionPane.showMessageDialog(null, "Lincenca invalida!");
                return false;
            }

            if (!sintese.equals(Register.getSintese())) {
                JOptionPane.showMessageDialog(null, "O programa principal foi alterado! Lincença inválida!");
                return false;
            }

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateFormat.parse(data));

            if (cal.before(Calendar.getInstance())) {
                JOptionPane.showMessageDialog(null, "Validade da licença já foi ultrassada.");
                return false;
            }

            verificar = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return verificar;
    }

    public void readLicenca() {
        try {
            int tolerancia = 0;
            File inputFile = new File("Licenca");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("Registo");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    mac = eElement.getElementsByTagName("Mac").item(0).getTextContent();
                    machineId = eElement.getElementsByTagName("Machine-id").item(0).getTextContent();
                    nomeUser = eElement.getElementsByTagName("Nome").item(0).getTextContent();
                    bi = eElement.getElementsByTagName("BI").item(0).getTextContent();
                    sintese = eElement.getElementsByTagName("Sintese").item(0).getTextContent();
                    data = eElement.getElementsByTagName("DataExpira").item(0).getTextContent();
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private String getMac() {

        InetAddress ip;
        String result = "";
        try {
            ip = InetAddress.getLocalHost();
            System.out.println("Current IP address : " + ip.getHostAddress());

            Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces();
            while (networks.hasMoreElements()) {
                NetworkInterface network = networks.nextElement();
                byte[] mac = network.getHardwareAddress();

                if (mac != null) {
                    System.out.print("Current MAC address : ");

                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < mac.length; i++) {
                        sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
                    }
                    System.out.println(sb.toString());
                    result = sb.toString();
                }
            }
        } catch (UnknownHostException | SocketException e) {
        }
        return result;
    }

    private String getMachineId() {

        String result = "";

        try {
            FileInputStream typeCpu = new FileInputStream("/var/lib/dbus/machine-id");
            byte[] cpu = new byte[typeCpu.available()];
            typeCpu.read(cpu);
            typeCpu.close();

            result = new String(Files.readAllBytes(Paths.get("/var/lib/dbus/machine-id")));

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;

    }

    @Override
    public String toString() {
        String str = "Programa: " + nomeDaApp
                + "\nVersão: " + versao + "\n";
            str += "\nLicença:"
                    + "\n\tUtilizador:"
                    + "\n\tNome: " + nomeUser
                    + "\n\tBI: " + bi
                    + "\n"
                    + "\n\tComputador: "
                    + "\n\tMac: " + mac
                    + "\n\tMachine ID: " + machineId
                    + "\n"
                    + "\n\tSintese: " + sintese
                    + "\n"
                    + "\n\tValidade Licença: " + data + "\n\n\n\n\n";

        return "\n" + str;
    }
}
