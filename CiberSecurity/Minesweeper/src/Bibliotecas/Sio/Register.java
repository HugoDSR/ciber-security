package Bibliotecas.Sio;

import Bibliotecas.Cifras.Cifras;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.SecretKey;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import Minesweeper.PathDropbox;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Register extends JFrame {

    private JLabel txtNome;
    private JLabel txtBI;
    private String mac;
    private String macId;

    public Register(String mac, String macId) {

        this.mac = mac;
        this.macId = macId;

        setBounds(100, 100, 500, 343);
        setTitle("Novo Registo");
        setLocationRelativeTo(null);

        getContentPane().setLayout(null);

        // Header Title
        JLabel hRegister = new JLabel("Tem que existir uma conexão à Internet para efetuar registo");
        hRegister.setFont(new Font("Tahoma", Font.BOLD, 11));
        hRegister.setHorizontalAlignment(SwingConstants.CENTER);
        hRegister.setBounds(20, 11, 400, 20);
        getContentPane().add(hRegister);

        JLabel hName = new JLabel("Nome :");
        hName.setBounds(78, 52, 89, 14);
        getContentPane().add(hName);

        JLabel hBI = new JLabel("Número BI :");
        hBI.setBounds(78, 84, 89, 14);
        getContentPane().add(hBI);

        // Name
        txtNome = new JLabel(Cifras.getNomeCC());
        txtNome.setBounds(217, 47, 250, 20);
        getContentPane().add(txtNome);

        // BI
        txtBI = new JLabel(Cifras.getNumeroCC());
        txtBI.setBounds(217, 77, 176, 20);
        getContentPane().add(txtBI);

        // Save Button
        JButton btnSave = new JButton("Registar");
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                int dialogButton = JOptionPane.YES_NO_OPTION;

                int dialogResult = JOptionPane.showConfirmDialog(null, "Assinar o Registo para finalizar processo?\n\n"
                        + "\nLicença:"
                        + "\n\tUtilizador:"
                        + "\n\tNome: " + txtNome.getText()
                        + "\n\tBI: " + txtBI.getText()
                        + "\n"
                        + "\n\tComputador: "
                        + "\n\tMac: " + mac
                        + "\n\tMachine ID: " + macId
                        + "\n\n\n"
                );

                if (dialogResult == JOptionPane.YES_OPTION) {
                    new File(PathDropbox.path + "Minesweeper-Licenca/PedidoRegisto/" + txtBI.getText()).mkdirs();
                    creatXml();
                    setSCifra();
                    setACifra();
                    setAssinatura();
                    Cifras.gerarKeyPair("PrivateKey", PathDropbox.path + "Minesweeper-Licenca/PedidoRegisto/" + txtBI.getText() + "/PublicKey");

                    JOptionPane.showMessageDialog(null, "Registo Completo");
                    closeWindow();
                } else if (dialogResult == JOptionPane.NO_OPTION) {
                    JOptionPane.showMessageDialog(null, "Registo Cancelado");
                    closeWindow();
                }
            }
        });
        btnSave.setBounds(161, 227, 100, 23);
        getContentPane().add(btnSave);

        setVisible(true);

    }

    public void setAssinatura() {

        try {
            String certificado = PathDropbox.path + "Minesweeper-Licenca/PedidoRegisto/" + txtBI.getText() + "/Certificado";
            String assinatura = PathDropbox.path + "Minesweeper-Licenca/PedidoRegisto/" + txtBI.getText() + "/Assinatura";

            FileInputStream fileIn = new FileInputStream(PathDropbox.path + "Minesweeper-Licenca/PedidoRegisto/" + txtBI.getText() + "/Regist");
            byte[] registo = new byte[fileIn.available()];
            fileIn.read(registo);

            Cifras.assinarFile(registo, certificado, assinatura);
        } catch (IOException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setSCifra() {

        FileInputStream fileIn = null;
        try {
            fileIn = new FileInputStream(PathDropbox.path + "Minesweeper-Licenca/PedidoRegisto/" + txtBI.getText() + "/Regist");
            byte[] cif = new byte[fileIn.available()];
            fileIn.read(cif);
            SecretKey key = Cifras.gerarKey(PathDropbox.path + "Minesweeper-Licenca/PedidoRegisto/" + txtBI.getText() + "/Key");
            Cifras.sCifrar(cif, key, PathDropbox.path + "Minesweeper-Licenca/PedidoRegisto/" + txtBI.getText() + "/Regist");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileIn.close();
            } catch (IOException ex) {
                Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void setACifra() {

        try {
            FileInputStream fileIn = new FileInputStream(PathDropbox.path + "Minesweeper-Licenca/PedidoRegisto/" + txtBI.getText() + "/Key");
            byte[] cif = new byte[fileIn.available()];
            fileIn.read(cif);

            FileInputStream pubKey = new FileInputStream("pubKeyFile");
            byte[] pk = new byte[pubKey.available()];
            pubKey.read(pk);

            Cifras.aCifrarPubic(cif, pk, PathDropbox.path + "Minesweeper-Licenca/PedidoRegisto/" + txtBI.getText() + "/Key");

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void creatXml() {

        try {

            BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            Element rootElement = document.createElement("Registo");
            document.appendChild(rootElement);

            //preechimento de xml
            Element em1 = document.createElement("Mac");
            em1.appendChild(document.createTextNode(getTxtMac()));
            rootElement.appendChild(em1);

            Element em2 = document.createElement("Machine-id");
            em2.appendChild(document.createTextNode(getTxtMacId()));
            rootElement.appendChild(em2);

            Element em3 = document.createElement("Nome");
            em3.appendChild(document.createTextNode(getTxtNome()));
            rootElement.appendChild(em3);

            Element em4 = document.createElement("BI");
            em4.appendChild(document.createTextNode(getTxtBI()));
            rootElement.appendChild(em4);

            Element em5 = document.createElement("Sintese");
            em5.appendChild(document.createTextNode(getSintese()));
            rootElement.appendChild(em5);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);

            StreamResult result = new StreamResult(new StringWriter());
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "5");
            transformer.transform(source, result);

            FileOutputStream fop = null;
            File file;
            try {

                file = new File(PathDropbox.path + "Minesweeper-Licenca/PedidoRegisto/" + txtBI.getText() + "/Regist");
                fop = new FileOutputStream(file);
                if (!file.exists()) {
                    file.createNewFile();
                }

                String xmlString = result.getWriter().toString();
                System.out.println(xmlString);
                byte[] contentInBytes = xmlString.getBytes();

                fop.write(contentInBytes);
                fop.flush();
                fop.close();

                System.out.println("Done");

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (fop != null) {
                        fop.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {

        }

    }

    public String getTxtNome() {
        return txtNome.getText();
    }

    protected String getTxtBI() {
        return txtBI.getText();
    }

    public String getTxtMac() {
        return this.mac;
    }

    public String getTxtMacId() {
        return this.macId;
    }

    public static String getSintese() {
        String result = "";
        try {
            FileInputStream fileIn = new FileInputStream("src/Minesweeper/Minesweeper.java");

            byte[] sintese = new byte[fileIn.available()];
            fileIn.read(sintese);
            result = Cifras.funcSintese(sintese, "SHA-256");
        } catch (IOException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    private void closeWindow() {
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }
}
