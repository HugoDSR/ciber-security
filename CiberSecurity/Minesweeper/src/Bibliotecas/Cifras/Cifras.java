package Bibliotecas.Cifras;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.ProviderException;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class Cifras {

    //Função para gerar chave simetrica
    //Escreve a chave num documento e devolve tambem como vareavel de saida
    public static SecretKey gerarKey(String path)  {

        FileOutputStream fileOut = null;
        SecretKey key = null;
        try {
            fileOut = new FileOutputStream(path);
            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            SecretKey sKey = keyGen.generateKey();
            fileOut.write(sKey.getEncoded());
            fileOut.close();
            key = sKey;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileOut.close();
            } catch (IOException ex) {
                Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return key;
    }

    //Função para gerar chaves assimetricas
    //Escreve cada chave num documento.
    public static void gerarKeyPair(String priv, String publ)  {

        try {
            KeyPairGenerator kpGen = KeyPairGenerator.getInstance("RSA");
            //tamanha da chave é 1024
            kpGen.initialize(1024);
            
            KeyPair keyPar = kpGen.genKeyPair();
            
            PublicKey pubKey = keyPar.getPublic();
            PrivateKey privKey = keyPar.getPrivate();
            
            FileOutputStream privFileOut = new FileOutputStream(priv);
            FileOutputStream pubFileOut = new FileOutputStream(publ);
            
            privFileOut.write(privKey.getEncoded());
            privFileOut.close();
            pubFileOut.write(pubKey.getEncoded());
            pubFileOut.close();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Cifra um documento dado como variavel de entrada usando uma cifra simetrica,
    //a chave é dada como dada de entrada
    //a mensagem cifrada é guardada num file dado como argumento de entrada output
    public static void sCifrar(byte[] file, SecretKey sKey, String outputPath)  {

        try {
            Cipher cifra = Cipher.getInstance("AES");
            cifra.init(Cipher.ENCRYPT_MODE, sKey);
            FileOutputStream fileOut = new FileOutputStream(outputPath);
            fileOut.write(cifra.doFinal(file));
            fileOut.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Decifra um documento cifrado dado como vareavel de entrada
    //a chave é dada como dada de entrada e a mensagem decifrada é guardada num file
    public static void sDecifrar(byte[] file, byte[] sKey, String outputPath)  {
        try {
            SecretKeySpec spec = new SecretKeySpec(sKey, "AES");
            Cipher cifra = Cipher.getInstance("AES");
            cifra.init(Cipher.DECRYPT_MODE, spec);
            FileOutputStream fileOut = new FileOutputStream(outputPath);
            fileOut.write(cifra.doFinal(file));
            fileOut.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //Cifra um documento de forma assimetrica usando uma chave publica
    //O ficheiro a cifrar é dado como vareavel de entrada assim como o ficheiro que contem a chave publica
    public static void aCifrarPubic(byte[] file, byte[] pubKey, String path)  {

        try {
            X509EncodedKeySpec x509Spec = new X509EncodedKeySpec(pubKey);
            KeyFactory keyF = KeyFactory.getInstance("RSA");
            PublicKey publicKey2 = keyF.generatePublic(x509Spec);
            Cipher cifra = Cipher.getInstance("RSA");
            cifra.init(Cipher.ENCRYPT_MODE, publicKey2);
            FileOutputStream fileoutput = new FileOutputStream(path);
            fileoutput.write(cifra.doFinal(file));
            fileoutput.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } 

    }
    
    public static void aCifrarPrivate(byte[] file, byte[] privKey, String path)  {

        try {
            PKCS8EncodedKeySpec x509Spec = new PKCS8EncodedKeySpec(privKey);
            KeyFactory keyF = KeyFactory.getInstance("RSA");
            PrivateKey privateKey2 = keyF.generatePrivate(x509Spec);
            Cipher cifra = Cipher.getInstance("RSA");
            cifra.init(Cipher.ENCRYPT_MODE, privateKey2);
            FileOutputStream fileoutput = new FileOutputStream(path);
            fileoutput.write(cifra.doFinal(file));
            fileoutput.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //Decifra um documento de forma assimetrica usando uma chave privada
    //O ficheiro a cifrar é dado como vareavel de entrada assim como o ficheiro que contema  chave privada
    public static void aDecifrarPriv(byte[] file, byte[] privKey, String outputPath) {

        try {
            PKCS8EncodedKeySpec pkcs8Spec = new PKCS8EncodedKeySpec(privKey);
            KeyFactory keyF = KeyFactory.getInstance("RSA");
            PrivateKey privateKey2 = keyF.generatePrivate(pkcs8Spec);
            Cipher cifra = Cipher.getInstance("RSA");
            cifra.init(Cipher.DECRYPT_MODE, privateKey2);
            FileOutputStream fileoutput = new FileOutputStream(outputPath);
            fileoutput.write(cifra.doFinal(file));
            fileoutput.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void aDecifrarPub(byte[] file, byte[] pubKey, String outputPath) {

        try {
            X509EncodedKeySpec pkcs8Spec = new X509EncodedKeySpec(pubKey);
            KeyFactory keyF = KeyFactory.getInstance("RSA");
            PublicKey publicKey2 = keyF.generatePublic(pkcs8Spec);
            Cipher cifra = Cipher.getInstance("RSA");
            cifra.init(Cipher.DECRYPT_MODE, publicKey2);
            FileOutputStream fileoutput = new FileOutputStream(outputPath);
            fileoutput.write(cifra.doFinal(file));
            fileoutput.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Cria uma sintese de um ficheiro dado
    //O ficheiro a sintetisar é dado como argumento input e o resultado e dado num file output
    public static String funcSintese(byte[] input, String sFunc){
        
        String hexString="";
        
        try {
          
            MessageDigest md = MessageDigest.getInstance(sFunc);

            byte[] toDigest = md.digest(input);
            
        
            hexString = DatatypeConverter.printHexBinary(toDigest);

        } catch (NoSuchAlgorithmException e) {
            System.out.print("Erro: "+ e);
        }
        
        return hexString;
        
    }

    
    //message = mensagem a ser cifrada
    //pathCertificado = caminho e nome do ficheiro que contem o certificado do cc
    //pathAssinatra = caminho e nome do ficheiro que contem o certificado do cc
    public static void assinarFile(byte[] message, String pathCertificado, String pathAssinatura) {
        try {
            Provider prov = Security.getProvider("SunPKCS11-CartaoCidadao");

            KeyStore ks = KeyStore.getInstance("PKCS11", prov);

            ks.load(null, null);

            Signature assinatura = Signature.getInstance("SHA1withRSA", prov);
            //SHA256withRSA
            char[] password = "0".toCharArray();
            PrivateKey chaveprivada = (PrivateKey) ks.getKey("CITIZEN SIGNATURE CERTIFICATE", password);

            assinatura.initSign(chaveprivada);
            assinatura.update(message);

            Certificate certificado = ks.getCertificate("CITIZEN SIGNATURE CERTIFICATE");

            FileOutputStream fileoutcertif = new FileOutputStream(pathCertificado);
            fileoutcertif.write(certificado.getEncoded());
            fileoutcertif.close();

            FileOutputStream fileoutdocumento = new FileOutputStream(pathAssinatura);
            fileoutdocumento.write(assinatura.sign());
            fileoutdocumento.close();

        } catch (ProviderException ex) {
            System.out.println("Nao foi possivel ler o Cartao de Cidadao");
        } catch (KeyStoreException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnrecoverableKeyException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //message = Documento Assinado
    //assinatura_ficheiro = Assinatura
    public static boolean verificarAss(byte[] message, byte[] assinatura_ficheiro, String pathCertificado) {
        boolean validar = false;
        FileInputStream certificado_ficheiro = null;
        try {
            certificado_ficheiro = new FileInputStream(pathCertificado);
            CertificateFactory cf = CertificateFactory.getInstance("X509");
            Certificate cert = cf.generateCertificate(certificado_ficheiro);
            X509Certificate certx509 = (X509Certificate) cert;
            // Nome da entidade do Certificado
            Signature assinatura = Signature.getInstance("SHA1withRSA"); //SHA256withRSA
            assinatura.initVerify(cert.getPublicKey());
            assinatura.update(message);
            validar = assinatura.verify(assinatura_ficheiro);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                certificado_ficheiro.close();
            } catch (IOException ex) {
                Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return validar;

    }

    public static String getNomeCC() {
        String nome = new String();
        try {
            Provider prov = Security.getProvider("SunPKCS11-CartaoCidadao");
            
            KeyStore ks = KeyStore.getInstance("PKCS11", prov);
            
            ks.load(null, null);
            
            Certificate cert = ks.getCertificate("CITIZEN SIGNATURE CERTIFICATE");
            
            X509Certificate certx509 = (X509Certificate) cert;
            String info = certx509.getSubjectDN().getName();
            String[] info2 = info.split(",");
            
            nome = info2[0].split("=")[1];
        } catch (KeyStoreException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nome;
    }
    
    public static String getNumeroCC() {
        String bi = new String();
        try {
            Provider prov = Security.getProvider("SunPKCS11-CartaoCidadao");
            
            KeyStore ks = KeyStore.getInstance("PKCS11", prov);
            
            ks.load(null, null);
            
            Certificate cert = ks.getCertificate("CITIZEN SIGNATURE CERTIFICATE");
            
            X509Certificate certx509 = (X509Certificate) cert;
            String info = certx509.getSubjectDN().getName();
            String[] info2 = info.split(",");
            bi = info2[1].split("=BI")[1];
        } catch (KeyStoreException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(Cifras.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bi;
    }
    
    public static boolean isSmartCardIn() {
        Provider prov = Security.getProvider("SunPKCS11-CartaoCidadao");
        try {
            KeyStore ks = KeyStore.getInstance("PKCS11", prov);
        } catch (KeyStoreException ex) {
            return false;
        }
        return true;
    }
}
